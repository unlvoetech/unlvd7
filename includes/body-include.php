<div id="portalNav">
<div id="center">

<div class="PN_leftCol">
    <a href="http://go.unlv.edu" title="UNLV"><img src="http://go.unlv.edu/global/images/logo.png" border="0" width="58" height="21" alt="UNLV" /></a>
</div> 
<!-- end .PN_leftCol -->

<div class="PN_rightCol">

<ul id="qm0" class="qmmc">

    <li><a class="qmparent" href="javascript:void(0)" onmouseover="toggleMenu('audience_dropdown',true);" onmouseout="toggleMenu('audience_dropdown',false);" title="Information For">Information&nbsp;For&nbsp;&raquo;</a>

        <ul id="audience_dropdown" onmouseover="toggleMenu('audience_dropdown',true);" onmouseout="toggleMenu('audience_dropdown',false);">
            <li><a href="http://go.unlv.edu/futurestudents" title="Future Students">Future Students</a></li>
            <li><a href="http://go.unlv.edu/currentstudents" title="Current Students">Current Students</a></li>
            <li><a href="http://go.unlv.edu/alumni" title="Alumni">Alumni</a></li>
            <li><a href="http://go.unlv.edu/facultystaff" title="Faculty/Staff">Faculty/Staff</a></li>
            <li><a href="http://go.unlv.edu/donors" title="Donors">Donors</a></li>

            <li><a href="http://go.unlv.edu/community" title="Community">Community</a></li>
        </ul>
    </li>
    
    <li><a class="qmparent" href="javascript:void(0)" onmouseover="toggleMenu('topic_dropdown',true);" onmouseout="toggleMenu('topic_dropdown',false);" title="Topics">Topics&nbsp;&raquo;</a>
        <ul id="topic_dropdown" onmouseover="toggleMenu('topic_dropdown',true);" onmouseout="toggleMenu('topic_dropdown',false);">
            <li><a href="http://go.unlv.edu/about" title="About UNLV">About UNLV</a></li>
            <li><a href="http://go.unlv.edu/about/academics" title="Academics">Academics</a></li>

            <li><a href="http://go.unlv.edu/about/administration" title="Administration">Administration</a></li>
            <li><a href="http://www.unlv.edu/admissions" title="Admissions">Admissions</a></li>
            <li><a href="http://go.unlv.edu/about/athletics" title="Athletics">Athletics</a></li>
            <li><a href="http://foundation.unlv.edu/give.html" title="Give to UNLV">Give to UNLV</a></li>
            <li><a href="http://library.unlv.edu/" title="Libraries">Libraries</a></li>
            <li><a href="http://research.unlv.edu/" title="Research">Research</a></li>

        </ul>
    </li>
    
    <li><a class="qmparent" href="javascript:void(0)" onmouseover="toggleMenu('quicklinks_dropdown',true);" onmouseout="toggleMenu('quicklinks_dropdown',false);" title="Quick Links">Quick&nbsp;Links&nbsp;&raquo;</a>
        <ul id="quicklinks_dropdown" onmouseover="toggleMenu('quicklinks_dropdown',true);" onmouseout="toggleMenu('quicklinks_dropdown',false);">
            <li><a href="http://unlv.bncollege.com/" title="Bookstore">Bookstore</a></li>
            <li><a href="http://library.unlv.edu/" title="Libraries">Libraries</a></li>
            <li><a href="http://lotusnotes.oit.unlv.edu/" title="Lotus Notes">Lotus Notes</a></li>

            <li><a href="http://go.unlv.edu/maps" title="Maps &amp; Parking">Maps &amp; Parking</a></li>
            <li><a href="https://my.unlv.nevada.edu/" title="MyUNLV">MyUNLV</a></li>
            <li><a href="http://rebelcard.unlv.edu/" title="RebelCard">RebelCard</a></li>
            <li><a href="http://rebelmail.unlv.edu/" title="Rebelmail">Rebelmail</a></li>
            <li><a href="https://webcampus.nevada.edu/webct/entryPage.dowebct" title="WebCampus">WebCampus</a></li>

        </ul>
    </li>
    
    <li>
        <form method="get" action="http://search.nscee.edu/search" name="searchform" id="searchform">
            <input type="text" class="sf_text" name="q" id="qt" maxlength="255" value="Search UNLV" title="Search UNLV" onclick="document.searchform.qt.value=''" /><input type="submit" class="sf_submit" name="btnG" id="btnG" value="Search" />
            <input type="hidden" name="site" value="default_collection" />
            <input type="hidden" name="client" value="default_frontend" />
            <input type="hidden" name="proxystylesheet" value="default_frontend" />
            <input type="hidden" name="output" value="xml_no_dtd" /> 
        </form>

    </li>
    
    <li class="qmclear">&nbsp;</li>

</ul>

</div>
<!-- end PN_rightCol -->

<div class="PN_clear"></div>

</div>
<!--end #center-->

</div>
<!-- end portalNav -->
<link rel="shortcut icon" type="image/x-icon" href="http://go.unlv.edu/global/images/favicon.ico" />

<link type="text/css" rel="stylesheet" media="all" href="http://web.unlv.edu/Include/header/css/styles.css" />

<link href="http://web.unlv.edu/templates/css/style.css" type="text/css" rel="stylesheet" media="all" />

<!--[if IE 6]>
<script type="text/javascript" src="http://www.unlv.edu/Include/header/js/toggleMenu.js"></script>
<style type="text/css" media="all">
.qmmc li a.qmparent {
    margin-bottom:-1px;
    border:none;
}
#searchform {
    width:175px;
}
#searchform input.sf_text {
    text-align:left;
    color:#333;
    }
#searchform input.sf_submit {
    margin-top:-3px;
	}
</style>
<link type="text/css" rel="stylesheet" media="all" href="http://www.unlv.edu/templates/css/style-ie6.css" />
<![endif]-->

<!--[if IE 7]>
<style type="text/css" media="all">
#searchform input.sf_submit {
    padding:0;
}
</style>
<link type="text/css" rel="stylesheet" media="all" href="http://www.unlv.edu/templates/css/style-ie7.css" />
<![endif]-->

<link href="css/custom.css" type="text/css" rel="stylesheet" media="all" /> 
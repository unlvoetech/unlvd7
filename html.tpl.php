<?php
// $Id: html.tpl.php,v 1.6 2010/11/24 03:30:59 webchick Exp $

/**
 * @file
 * Default theme implementation to display the basic html structure of a single
 * Drupal page.
 *
 * Variables:
 * - $css: An array of CSS files for the current page.
 * - $language: (object) The language the site is being displayed in.
 *   $language->language contains its textual representation.
 *   $language->dir contains the language direction. It will either be 'ltr' or 'rtl'.
 * - $rdf_namespaces: All the RDF namespace prefixes used in the HTML document.
 * - $grddl_profile: A GRDDL profile allowing agents to extract the RDF data.
 * - $head_title: A modified version of the page title, for use in the TITLE
 *   tag.
 * - $head_title_array: (array) An associative array containing the string parts
 *   that were used to generate the $head_title variable, already prepared to be
 *   output as TITLE tag. The key/value pairs may contain one or more of the
 *   following, depending on conditions:
 *   - title: The title of the current page, if any.
 *   - name: The name of the site.
 *   - slogan: The slogan of the site, if any, and if there is no title.
 * - $head: Markup for the HEAD section (including meta tags, keyword tags, and
 *   so on).
 * - $styles: Style tags necessary to import all CSS files for the page.
 * - $scripts: Script tags necessary to load the JavaScript files and settings
 *   for the page.
 * - $page_top: Initial markup from any modules that have altered the
 *   page. This variable should always be output first, before all other dynamic
 *   content.
 * - $page: The rendered page content.
 * - $page_bottom: Final closing markup from any modules that have altered the
 *   page. This variable should always be output last, after all other dynamic
 *   content.
 * - $classes String of classes that can be used to style contextually through
 *   CSS.
 *
 * @see template_preprocess()
 * @see template_preprocess_html()
 * @see template_process()
 */
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
  "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="<?php print $grddl_profile; ?>">
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <?php print $styles; ?>
  <?php print $scripts; ?>

  <?php //Modified for unlvD7 theme
        include("includes/headcode-include.php"); ?>
  <style type="text/css">
    #block-user-login .content {width: 162px; min-height: 140px; margin-left: 15px;}
    #block-user-login h2,
    .region-sidebar-first h2
    {
        font:normal normal 1.05em "Times New Roman", Times, serif;
        text-transform: uppercase;
        background-color:#cecece;
        padding:3px;
    }

    #block-user-login .content a  {font-size: 11px; text-decoration: none;}
    #block-user-login .item-list li.first,
    #block-user-login .item-list li.last,
    .region-sidebar-first ul.menu li.first,
    .region-sidebar-first ul.menu li.last
    {
        list-style-type: none;
        list-style-image: none;
        margin-left: 5px;
        overflow: hidden;
    }

    .main-right { width: 200px;}
    #sidebar-second .content { width: 155px; min-height: 200px; margin-right: 5px;}
#breadcrumb { width: 100%; background:url(http://www.unlv.edu/templates/css/images/bg-right.png) repeat-y; background-position: right top;}
    #breadcrumb .breadcrumb { width: 100%; margin-left: 15px;}
    #breadcrumb .breadcrumb a { margin-left: 15px; font-size: 10px; text-decoration: none;}
    #Wrapper .content .main.left .content{
	float:left;
	width:560px;
	padding:10px;
}
  </style>
</head>
<body>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>
</body>
</html>
